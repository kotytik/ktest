import firebase from 'firebase/app'
import 'firebase/firestore'
// Get a Firestore instance
const db = firebase
  .initializeApp({ projectId: "ktest-2e2ce" })
let firestore = db.firestore()
export default {
  getTask() {
    return firestore.collection("tasks").orderBy('dateCreate','desc')
  },
  removeTask(id){
    firestore.collection("tasks").doc(id).delete();
  },
  addTask(data){
    data.dateCreate = new Date().toISOString();
     return firestore.collection("tasks").add(data);

  }

}