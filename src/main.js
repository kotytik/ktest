import Vue from 'vue'
import App from './App.vue'

import 'firebase/firestore'
import { firestorePlugin } from 'vuefire'
import TextareaAutosize from 'vue-textarea-autosize'
 
Vue.use(TextareaAutosize)
Vue.use(firestorePlugin)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
